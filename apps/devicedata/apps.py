from django.apps import AppConfig


class DataConfig(AppConfig):
    name = 'apps.devicedata'
    verbose_name = u'Data'
