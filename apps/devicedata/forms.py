from django import forms
from django.core.exceptions import ValidationError

from .models import ChartUnit, ChartContent
from apps.devices.models import DeviceTypeRegister


class ChartUnitForm(forms.ModelForm):
    class Meta:
        model = ChartUnit
        fields = ['name', 'description']


class ChartContentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ChartContentForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance:
            self.fields['chart'].widget.attrs['hidden'] = True
            self.fields['chart'].label = ''

    def clean_chart(self):
        instance = getattr(self, 'instance', None)
        if instance:
            return instance.chart
        else:
            return self.cleaned_data['chart']

    def clean(self):
        cleaned_data = super().clean()
        device = cleaned_data.get("device")
        register = cleaned_data.get("register")

        if device and register:
            if len(list(DeviceTypeRegister.objects.filter(device_type__device__id=device.id, id=register.id))) == 0:
                raise ValidationError('Register does not belong to this device')

    class Meta:
        model = ChartContent
        fields = ['chart', 'name', 'device', 'register', 'additive', 'coefficient', 'check_mask']
