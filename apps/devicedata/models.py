from django.db import models
from apps.devices.models import Device, DeviceTypeRegister


class ChartUnit(models.Model):
    u"""
    Saved prepared graph units to show with charts or tables device registers data
    """
    name = models.CharField(
        u'chart name',
        max_length=50,
        blank=False, null=False, unique=True
    )

    description = models.TextField(
        u'description of chart unit',
        max_length=300,
        blank=True, null=True
    )

    class Meta:
        verbose_name = u'chart unit'
        verbose_name_plural = u'chart units'
        db_table = 'poll_chart_units'

    def __str__(self):
        return u'{0}'.format(self.name)


class ChartContent(models.Model):
    u"""
    Chart contents
    """
    chart = models.ForeignKey(
        'ChartUnit', verbose_name=u'Chart unit', on_delete=models.CASCADE
    )

    device = models.ForeignKey(
        'devices.Device', verbose_name=u'Device', on_delete=models.CASCADE
    )

    register = models.ForeignKey(
        'devices.DeviceTypeRegister', verbose_name=u'Device register', on_delete=models.CASCADE
    )

    additive = models.FloatField(
        u'additive modifier',
        blank=True, null=True, default=0
    )

    coefficient = models.FloatField(
        u'coefficient modifier',
        blank=True, null=True, default=1
    )

    check_mask = models.PositiveIntegerField(
        u'check logical equality with mask, bool(x & mask), if true => alert',
        help_text=u'positive integer value',
        blank=True, null=True
    )

    name = models.CharField(
        u'chart element name',
        max_length=60,
        help_text=u'chart element name',
        blank=True, null=True
    )

    class Meta:
        verbose_name = u'chart content'
        verbose_name_plural = u'chart contents'
        db_table = 'poll_chart_contents'

    def __str__(self):
        if self.name:
            return u'{0}'.format(self.name)
        else:
            return u'{0}-{1} [{2};{3};{4}]'.format(self.device.name, self.register.name,
                                                   self.additive, self.coefficient, self.check_mask)
