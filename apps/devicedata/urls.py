from django.urls import path
from . import views


app_name = 'devicedata'

urlpatterns = [
    path('data/', views.view_data, name='view_data'),
    path('data_modified/', views.view_data_modified, name='view_data_modified'),
    path('data/devices/filtered/', views.filter_devices, name='filter_devices'),
    path('data/registers/filtered/', views.filter_registers, name='filter_registers'),
    path('data/filtered/', views.filter_data, name='filter_data'),
    path('data/filtered_modified/', views.filter_data_modified, name='filter_data_modified'),
    path('data/charts/', views.charts, name='charts_list'),
    path('data/charts/new/', views.chart_detail, name='chart_new'),
    path('data/charts/<int:chart_id>/', views.chart_detail, name='chart_detail'),
    path('data/charts/<int:chart_id>/delete/', views.chart_delete, name='chart_delete'),
    path('data/charts/<int:chart_id>/series/', views.chart_series_list, name='series_list'),
    path('data/charts/<int:chart_id>/series/new/', views.series_detail, name='series_new'),
    path('data/charts/<int:chart_id>/series/<int:series_id>/', views.series_detail, name='series_detail'),
    path('data/charts/<int:chart_id>/series/<int:series_id>/delete/', views.series_delete, name='series_delete'),
]
