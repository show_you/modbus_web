import json

from modbus.settings import DATABASES
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.template import loader
from django.shortcuts import get_object_or_404, render

import psycopg2
from psycopg2 import Error

from apps.devices.models import Device, DeviceTypeRegister, ReadFunctionCodes, DeviceType
from .models import ChartUnit, ChartContent
from .forms import ChartUnitForm, ChartContentForm


def view_data(request):
    devices = list(Device.objects.order_by('port'))
    template = loader.get_template('devicedata/device_data.html')

    context = {
        'devices': devices,
    }

    return HttpResponse(template.render(context, request))


def view_data_modified(request):
    devices = list(Device.objects.order_by('port'))
    charts_list = list(ChartUnit.objects.order_by('name'))

    template = loader.get_template('devicedata/device_data_modified.html')

    context = {
        'devices': devices,
        'charts': charts_list,
    }

    return HttpResponse(template.render(context, request))


def filter_devices(request):
    devices = []
    if request.method == 'POST':
        name = request.POST.get('name', '')
        d_type = request.POST.get('type', '')
        comment = request.POST.get('comment', '')
        devices_list = list(Device.objects.filter(name__icontains=name, type__name__icontains=d_type,
                                                  comment__icontains=comment))

        for device in devices_list:
            devices.append({'id': device.id, 'name': device.name})

    return JsonResponse({'devices': devices})


def filter_registers(request):
    registers = []
    if request.method == 'POST':
        devices = request.POST.getlist('devices[]')
        name = request.POST.get('name', '')
        d_type = request.POST.get('type', '')
        comment = request.POST.get('comment', '')
        devices_list = list(Device.objects.filter(id__in=devices))
        device_types = [device.type.id for device in devices_list]
        registers_list = list(DeviceTypeRegister.objects.filter(name__icontains=name,
                                                                device_type__name__icontains=d_type,
                                                                comment__icontains=comment,
                                                                device_type_id__in=device_types))
        for register in registers_list:
            if register.function:
                registers.append({'id': register.id, 'name': str(register)})

    return JsonResponse({'registers': registers})


def apply_modifier(additive, coefficient, mask, data_point):
    if data_point is None:
        return data_point
    if mask:
        return int(bool(int((data_point + additive) * coefficient) & mask))
    else:
        return (data_point + additive) * coefficient


def filter_data(request):
    response_json = {'data': []}

    if request.method == 'POST':
        connection = 0
        cursor = 0

        try:
            devices = request.POST.getlist('devices[]')
            registers = json.loads(request.POST.get('registers'))
            time_from = request.POST.get('time_from', None)
            time_to = request.POST.get('time_to', None)
            interval = request.POST.get('interval', 0)

            register_params_list = []

            for r in registers:
                reg = dict()
                reg['id'] = int(r['id'])
                if 'additive' in r:
                    reg['additive'] = float(r['additive'])
                else:
                    reg['additive'] = 0
                if 'coefficient' in r:
                    reg['coefficient'] = float(r['coefficient'])
                else:
                    reg['coefficient'] = 1
                if 'mask' in r and r['mask']:
                    reg['mask'] = int(r['mask'])
                else:
                    reg['mask'] = None
                register_params_list.append(reg)

            register_ids = [r['id'] for r in register_params_list]

            devices_list = list(Device.objects.filter(id__in=devices))

            # print("Try to connect to DB")
            connection = psycopg2.connect(database=DATABASES['default']['NAME'], host=DATABASES['default']['HOST'],
                                          port=int(DATABASES['default']['PORT']),
                                          user=DATABASES['default']['USER'],
                                          password=DATABASES['default']['PASSWORD'])

            for device in devices_list:
                registers_list = list(DeviceTypeRegister.objects.filter(id__in=register_ids, device_type_id=device.type.id))
                register_names = []
                if len(registers_list) > 0:
                    statement = "SELECT datetime_point"
                    for register in registers_list:
                        statement += ", " + register.name
                        register_names.append({'id': register.id, 'name': register.name})
                    statement += " FROM " + device.type.table_name
                    statement += " WHERE device_id=" + str(device.id)
                    if time_from:
                        statement += " AND datetime_point>='" + time_from.replace('T', ' ') + "'"
                    if time_to:
                        statement += " AND datetime_point<'" + time_to.replace('T', ' ') + "'"
                    if int(interval):
                        statement += " AND datetime_point BETWEEN current_timestamp - interval '" + \
                                     str(interval) + "minutes' AND 'now'"
                    statement += " order by datetime_point;"
                    cursor = connection.cursor()
                    cursor.execute(statement)
                    identity = cursor.fetchall()
                    identity = list(map(list, zip(*identity)))
                    if len(identity) == 0:
                        identity.append([])
                        for reg_name in register_names:
                            identity.append([])
                    index = 1
                    # data_response = {'device_name': device.name, 'register_names': register_names, 'points': identity}
                    data_response = {'device_name': device.name,
                                     'registers': [{'name': 'DATETIME_POINT', 'points': identity[0]}]}
                    for register_name in register_names:
                        for register_param in register_params_list:
                            if register_param['id'] == register_name['id']:
                                name = register_name['name'] + ' [' + str(register_param['additive']) + ';' + \
                                       str(register_param['coefficient']) + ';' + str(register_param['mask']) + ']'
                                register = {'name': name,
                                            'points': [apply_modifier(register_param['additive'],
                                                                      register_param['coefficient'],
                                                                      register_param['mask'], i)
                                                       for i in identity[index]]}
                                data_response['registers'].append(register)
                        index += 1

                    response_json['data'].append(data_response)

                    cursor.close()

            if connection:
                connection.close()
        except Error as e:
            if cursor:
                cursor.close()
            if connection:
                connection.close()
            response_json['error'] = str(e)

    return JsonResponse(response_json)


def filter_data_modified(request):
    response_json = {'data': []}

    if request.method == 'POST':
        connection = 0
        cursor = 0

        try:
            series_list = json.loads(request.POST.get('series'))
            time_from = request.POST.get('time_from', None)
            time_to = request.POST.get('time_to', None)
            interval = request.POST.get('interval', 0)

            series_params_list = []

            for s in series_list:
                series = dict()
                series['register_id'] = int(s['register_id'])
                series['device_id'] = int(s['device_id'])
                series['series_name'] = s['series_name']
                if 'additive' in s:
                    series['additive'] = float(s['additive'])
                else:
                    series['additive'] = 0
                if 'coefficient' in s:
                    series['coefficient'] = float(s['coefficient'])
                else:
                    series['coefficient'] = 1
                if 'mask' in s and s['mask']:
                    series['mask'] = int(s['mask'])
                else:
                    series['mask'] = None
                series_params_list.append(series)

            # register_ids = [r['id'] for r in register_params_list]

            # devices_list = list(Device.objects.filter(id__in=devices))

            connection = psycopg2.connect(database=DATABASES['default']['NAME'], host=DATABASES['default']['HOST'],
                                          port=int(DATABASES['default']['PORT']),
                                          user=DATABASES['default']['USER'],
                                          password=DATABASES['default']['PASSWORD'])

            for series_params in series_params_list:
                device = Device.objects.get(id=series_params["device_id"])
                register = DeviceTypeRegister.objects.get(id=series_params["register_id"])

                statement = "SELECT datetime_point, " + register.name + " FROM " + device.type.table_name + \
                            " WHERE device_id=" + str(device.id)
                if time_from:
                    statement += " AND datetime_point>='" + time_from.replace('T', ' ') + "'"
                if time_to:
                    statement += " AND datetime_point<'" + time_to.replace('T', ' ') + "'"
                if int(interval):
                    statement += " AND datetime_point BETWEEN current_timestamp - interval '" + \
                                 str(interval) + "minutes' AND 'now'"
                statement += " order by datetime_point;"

                cursor = connection.cursor()
                cursor.execute(statement)
                identity = cursor.fetchall()
                identity = list(map(list, zip(*identity)))
                if len(identity) == 0:
                    identity.append([])
                    identity.append([])
                # data_response = {'device_name': device.name, 'register_names': register_names, 'points': identity}
                data_response = {'series_name': series_params["series_name"], 'time_points': identity[0]}
                data_response['data_points'] = [apply_modifier(series_params['additive'],
                                                               series_params['coefficient'],
                                                               series_params['mask'], i)
                                                for i in identity[1]]
                response_json['data'].append(data_response)

                cursor.close()

            if connection:
                connection.close()
        except Error as e:
            if cursor:
                cursor.close()
            if connection:
                connection.close()
            response_json['error'] = str(e)

    return JsonResponse(response_json)


def charts(request):
    charts_list = list(ChartUnit.objects.order_by('name'))
    template = loader.get_template('devicedata/charts_list.html')

    context = {
        'charts': charts_list,
    }

    return HttpResponse(template.render(context, request))


def chart_detail(request, chart_id=None):
    if chart_id:
        chart = get_object_or_404(ChartUnit, pk=chart_id)
        chart_name = chart.name
    else:
        chart = ChartUnit()
        chart_name = None

    if request.method == 'POST':
        form = ChartUnitForm(request.POST, instance=chart)

        if form.is_valid():
            chart.name = form.cleaned_data['name']
            chart.description = form.cleaned_data['description']

            # save into base
            chart.save()

            return HttpResponseRedirect(reverse('devicedata:charts_list'))
    else:
        form = ChartUnitForm(initial={'name': chart.name, 'description': chart.description})

    return render(request, 'devicedata/chart_detail.html', {'form': form, 'chart': chart, 'chart_title': chart_name})


def chart_series_list(request, chart_id):
    series_list = list(ChartContent.objects.filter(chart_id=chart_id).order_by('chart'))
    chart = list(ChartUnit.objects.filter(id=chart_id))[0]
    template = loader.get_template('devicedata/series_list.html')

    context = {
        'series_list': series_list,
        'chart': chart,
    }
    return HttpResponse(template.render(context, request))


def series_detail(request, chart_id, series_id=None):
    chart = get_object_or_404(ChartUnit, pk=chart_id)

    if series_id:
        series = get_object_or_404(ChartContent, pk=series_id)
        series_title = str(series)
    else:
        series = ChartContent()
        series.chart = chart
        series_title = None

    if request.method == 'POST':
        form = ChartContentForm(request.POST, instance=series, initial={'chart': chart})

        if form.is_valid():
            series.chart = form.cleaned_data['chart']
            series.name = form.cleaned_data['name']
            series.device = form.cleaned_data['device']
            series.register = form.cleaned_data['register']
            series.additive = form.cleaned_data['additive']
            series.coefficient = form.cleaned_data['coefficient']
            series.check_mask = form.cleaned_data['check_mask']

            # save into base
            series.save()

            return HttpResponseRedirect(reverse('devicedata:series_list', args=[chart_id, ]))
    else:
        if series_id:
            form = ChartContentForm(initial={'chart': chart, 'name': series.name, 'device': series.device,
                                             'register': series.register, 'additive': series.additive,
                                             'coefficient': series.coefficient, 'check_mask': series.check_mask})
        else:
            form = ChartContentForm(initial={'chart': chart, 'name': series.name, 'device': '',
                                             'register': '', 'additive': series.additive,
                                             'coefficient': series.coefficient, 'check_mask': series.check_mask})

    return render(request, 'devicedata/series_detail.html', {'form': form, 'series': series,
                                                             'series_title': series_title})


def chart_delete(request, chart_id):
    chart = get_object_or_404(ChartUnit, pk=chart_id)
    chart.delete()
    return HttpResponseRedirect(reverse('devicedata:charts_list'))


def series_delete(request, chart_id, series_id):
    series_object = get_object_or_404(ChartContent, pk=series_id)
    series_object.delete()

    return HttpResponseRedirect(reverse('devicedata:series_list', args=[chart_id, ]))
