from django.db import models
from apps.common.base_enum import BaseEnumerate


class PortTypes(BaseEnumerate):
    u"""
    Port types (for possible upgrade with other then Modbus RTU ports)
    """

    (
        NONE_TYPE, MODBUS_RTU, MODBUS_TCP
    ) = range(3)

    values = {
        NONE_TYPE: u'None recognizable type of port',
        MODBUS_RTU: u'RS232 port for Modbus RTU',
        MODBUS_TCP: u'Modbus TCP/IP'
    }


class Port(models.Model):
    u"""
    Ports of communication with devices
    """

    type = models.IntegerField(
        u'port type',
        choices=PortTypes.get_choices(),
        default=PortTypes.MODBUS_RTU
    )

    address = models.CharField(
        u'address of port, used to communicate',
        max_length=100,
        blank=False, null=False
    )

    speed = models.PositiveIntegerField(
        u'speed value for port communication',
        default=0
    )

    comment = models.TextField(
        u'additional comment',
        max_length=1000,
        blank=True, null=True
    )

    @property
    def type_verbose(self):
        u"""
        Get verbose value of port type
        :return: str
        """
        return u'{0}'.format(self.get_type_display())

    class Meta:
        verbose_name = u'port'
        verbose_name_plural = u'ports'
        # unique_together = ('address', 'speed')
        unique_together = ('type', 'address')
        ordering = ['type', 'address']
        db_table = 'poll_ports'

    def __str__(self):
        return u'{0}'.format(self.address)
