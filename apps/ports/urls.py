from django.urls import path
from . import views


app_name = 'ports'

urlpatterns = [
    path('ports/', views.ports, name='ports'),
    path('ports/<int:port_id>/', views.port_detail, name='port_detail'),
    path('ports/new/', views.port_detail, name='port_detail_new'),
    path('ports/<int:port_id>/delete/', views.port_delete, name='port_delete'),
]
