from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.template import loader
from django.shortcuts import get_object_or_404, render

from .forms import PortForm
from .models import Port, PortTypes
from apps.tasks.models import ReadTaskStatus, ReadTask, WriteTask, WriteTaskStatus


def ports(request):
    ports_list = list(Port.objects.order_by('address'))
    template = loader.get_template('ports/ports_list.html')

    for port in ports_list:
        port.type = PortTypes.values[port.type]

    context = {
        'ports_list': ports_list,
    }
    return HttpResponse(template.render(context, request))


def port_detail(request, port_id=None):
    if port_id:
        port = get_object_or_404(Port, pk=port_id)
        port_title = port.address
    else:
        port = Port()
        port.speed = 9600
        port_title = None

    if request.method == 'POST':
        form = PortForm(request.POST, instance=port)

        if form.is_valid():
            port.type = form.cleaned_data['type']
            port.address = form.cleaned_data['address']
            port.speed = form.cleaned_data['speed']
            port.comment = form.cleaned_data['comment']

            # print(port.id)
            # print(port.type)
            # print(port.address)
            # print(port.speed)
            # print(port.comment)

            # save into base
            port.save()

            return HttpResponseRedirect(reverse('ports:ports'))
    else:
        form = PortForm(initial={'type': port.type, 'address': port.address, 'speed': port.speed,
                                 'comment': port.comment})

    return render(request, 'ports/port_detail.html', {'form': form, 'port': port, 'port_title': port_title})


def port_delete(request, port_id):
    port = get_object_or_404(Port, pk=port_id)

    read_tasks = ReadTask.objects.filter(device__port_id=port.id, status=ReadTaskStatus.ENABLED)
    for task in read_tasks:
        task.status = ReadTaskStatus.DISABLED
        task.save()

    write_tasks = WriteTask.objects.filter(device__port_id=port.id, status=WriteTaskStatus.ENABLED)
    for task in write_tasks:
        task.status = WriteTaskStatus.DISABLED
        task.save()

    port.delete()

    return HttpResponseRedirect(reverse('ports:ports'))
