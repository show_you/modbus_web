from django import forms

from .models import Port, PortTypes


class PortForm(forms.ModelForm):
    class Meta:
        model = Port
        fields = ['type', 'address', 'speed', 'comment']
