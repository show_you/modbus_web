# coding=utf-8

class BaseEnumerate(object):
    """
    Базовый класс для создания перечислений.
    """
    # В словаре values описываются перечисляемые константы
    # и их человеческое название
    # Например: {STATE1: u'Состояние 1', CLOSED: u'Закрыто'}
    values = {}

    @classmethod
    def get_choices(cls):
        """
        Используется для ограничения полей ORM
        """
        return cls.values.items()

    @classmethod
    def get_constant_value_by_name(cls, name):
        """
        Возвращает значение атрибута константы, которая используется в
        качестве ключа к словарю values
        """
        if not isinstance(name, str):
            raise TypeError('"name" must be a string')

        if not name:
            raise ValueError('"name" must not be empty')

        return cls.__dict__[name]

    @classmethod
    def get_key_by_verbose(cls, verbose_name):
        """
        Возвращает значение атрибута константы, которая используется в
        качестве значения словаря values
        (по-сути возвращение ключа по значению)
        """
        if not isinstance(verbose_name, str):
            raise TypeError('"verbose_name" must be a string')

        if not verbose_name:
            raise ValueError('"verbose_name" must not be empty')

        for key, value in cls.values.items():
            if value == verbose_name:
                return key

        raise ValueError('No such verbose_name')

    @classmethod
    def get_all_keys(cls):
        return list(cls.values.keys())
