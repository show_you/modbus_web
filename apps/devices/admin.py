from django.contrib import admin
from .models import Device, DeviceType, DeviceTypeRegister


admin.site.register(DeviceType)
admin.site.register(Device)
admin.site.register(DeviceTypeRegister)
