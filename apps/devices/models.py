from django.db import models
from apps.common.base_enum import BaseEnumerate
from apps.ports.models import Port
from apps.tasks.models import ErrorReportPlans


class RecordTypes(BaseEnumerate):
    u"""
    Explains how to record data from devices
    """

    (
        DONT_RECORD, DB_RECORD, FILE_RECORD
    ) = range(3)

    values = {
        DONT_RECORD: u'Dont record data from device',
        DB_RECORD: u'Save data from device into database',
        FILE_RECORD: u'Save data from device into file'
    }


class GlobalReportingStatus(BaseEnumerate):
    u"""
    Reporting errors status
    """

    (
        DONT_REPORT, REPORT
    ) = range(2)

    values = {
        DONT_REPORT: u'Disable global device error reporting',
        REPORT: u'Enable global device error reporting'
    }


class ReadFunctionCodes(BaseEnumerate):
    u"""
    Function codes for read register procedures
    """
    NO_READ = 0
    READ_COIL_STATUS = 1
    READ_DISCRETE_INPUTS = 2
    READ_HOLDING_REGISTERS = 3
    READ_INPUT_REGISTERS = 4

    values = {
        NO_READ: u'no read function (0)',
        READ_COIL_STATUS: u'read coil status (1)',
        READ_DISCRETE_INPUTS: u'read discrete inputs (2)',
        READ_HOLDING_REGISTERS: u'read holding registers (3)',
        READ_INPUT_REGISTERS: u'read input registers (4)'
    }


class WriteFunctionCodes(BaseEnumerate):
    u"""
    Function codes for write register procedures
    """
    NO_WRITE = 0
    FORCE_SINGLE_COIL = 5
    PRESET_SINGLE_REGISTER = 6
    FORCE_MULTIPLE_COILS = 15
    PRESET_MULTIPLE_REGISTERS = 16

    values = {
        NO_WRITE: u'no write function (0)',
        FORCE_SINGLE_COIL: u'write single coil (5)',
        PRESET_SINGLE_REGISTER: u'write single register (6)',
        FORCE_MULTIPLE_COILS: u'write multiple coils (15)',
        PRESET_MULTIPLE_REGISTERS: u'write multiple registers (16)'
    }


class RegisterDataType(BaseEnumerate):
    u"""
    Data types of device registers
    """
    INTEGER = 2
    INT4 = 3
    FLOAT = 4

    values = {
        INTEGER: u'integer (2 byte)',
        INT4: u'integer (4 byte)',
        FLOAT: u'float (4 byte)'
    }


class DeviceType(models.Model):
    u"""
    Device type, needed for united storing data from devices of the same type
    """

    name = models.CharField(
        u'device type name',
        max_length=100,
        blank=False, null=False, unique=True
    )

    table_name = models.CharField(
        u'device data destination (e.g. table name, folder name...)',
        max_length=25,
        blank=False, null=False, unique=True
    )

    comment = models.TextField(
        u'additional comment',
        max_length=1000,
        blank=True, null=True
    )

    class Meta:
        verbose_name = u'device type'
        verbose_name_plural = u'device types'
        ordering = ['name']
        db_table = 'poll_device_types'

    def __str__(self):
        return u'{0}'.format(self.name)


class Device(models.Model):
    u"""
    Device to poll data from
    """
    name = models.CharField(
        u'device name',
        max_length=25,
        blank=False, null=False, unique=True
    )

    address = models.PositiveIntegerField(
        u'self address of device (id, integer)',
        default=0,
        help_text=u'positive integer',
        blank=False, null=False
    )

    port = models.ForeignKey(
        'ports.Port', verbose_name=u'Port', on_delete=models.SET_NULL, null=True
    )

    type = models.ForeignKey(
        'DeviceType', verbose_name=u'Device type', on_delete=models.SET_NULL, null=True
    )

    record_type = models.IntegerField(
        u'record type (how to store data from device)',
        choices=RecordTypes.get_choices(),
        default=RecordTypes.DONT_RECORD
    )

    comment = models.TextField(
        u'additional comment',
        max_length=1000,
        blank=True, null=True
    )

    class Meta:
        verbose_name = u'device'
        verbose_name_plural = u'devices'
        # unique_together = ('name', 'address')
        # unique_together = ('port', 'address')
        ordering = ['port', 'address', 'name']
        db_table = 'poll_devices'

    def __str__(self):
        return u'{0}:{1}'.format(self.name, self.address)


class DeviceTypeRegister(models.Model):
    u"""
    Register information, specific for some device type
    """

    device_type = models.ForeignKey(
        'DeviceType', verbose_name=u'Device type', on_delete=models.CASCADE
    )

    name = models.CharField(
        u'name of the register',
        max_length=25,
        blank=False, null=False
    )

    address = models.PositiveIntegerField(
        u'address of the current register in devices register map',
        default=0,
        help_text=u'positive integer',
        blank=False, null=False
    )

    function = models.PositiveIntegerField(
        u'read function',
        choices=ReadFunctionCodes.get_choices(),
        default=ReadFunctionCodes.NO_READ
    )

    write_function = models.PositiveIntegerField(
        u'write function',
        choices=WriteFunctionCodes.get_choices(),
        default=WriteFunctionCodes.NO_WRITE
    )

    byte_count = models.PositiveIntegerField(
        u'byte count for register value',
        choices=RegisterDataType.get_choices(),
        default=RegisterDataType.INTEGER,
    )

    comment = models.TextField(
        u'additional comment',
        max_length=1000,
        blank=True, null=True
    )

    class Meta:
        verbose_name = u'register'
        verbose_name_plural = u'registers'
        # unique_together = ('device_type', 'name', 'address')
        unique_together = (('device_type', 'name'), ('device_type', 'address'),)
        ordering = ['device_type', 'name', 'address']
        db_table = 'poll_registers'

    def __str__(self):
        return u'{0} - {1} ({2})'.format(self.device_type.name, self.name, self.address)


class DeviceSchemeLocation(models.Model):
    u"""
    Device scheme location for homepage
    """

    device = models.OneToOneField(
        'devices.Device', verbose_name=u'Device', on_delete=models.CASCADE, null=False
    )

    coordinateX = models.PositiveIntegerField(
        u'X coordinate (location on the scheme in px)',
        default=0,
        help_text=u'positive integer',
        blank=False, null=False
    )

    coordinateY = models.PositiveIntegerField(
        u'Y coordinate (location on the scheme in px)',
        default=0,
        help_text=u'positive integer',
        blank=False, null=False
    )

    class Meta:
        verbose_name = u'device scheme location'
        verbose_name_plural = u'device scheme locations'
        ordering = ['device']
        db_table = 'poll_scheme_locations'

    def __str__(self):
        return u'{0} (X{1}, Y{2})'.format(self.device, self.coordinateX, self.coordinateY)
