from django.urls import path
from . import views


app_name = 'devices'

urlpatterns = [
    path('devices/', views.devices, name='devices'),
    path('devices/<int:device_id>/', views.device_detail, name='device_detail'),
    path('devices/<int:device_id>/delete/', views.device_delete, name='device_delete'),
    path('devices/new/', views.device_detail, name='device_detail_new'),
    path('devicetypes/', views.device_types, name='device_types'),
    path('devicetypes/<int:device_type_id>/', views.device_type_detail, name='device_type_detail'),
    path('devicetypes/<int:device_type_id>/delete/', views.device_type_delete, name='device_type_delete'),
    path('devicetypes/new/', views.device_type_detail, name='device_type_detail_new'),
    path('devicetypes/<int:device_type_id>/registers/', views.device_type_registers, name='device_type_registers'),
    path('devicetypes/<int:device_type_id>/registers/<int:register_id>/', views.device_type_register_detail,
         name='device_type_register_detail'),
    path('devicetypes/<int:device_type_id>/registers/<int:register_id>/delete/', views.device_type_register_delete,
         name='device_type_register_delete'),
    path('devicetypes/<int:device_type_id>/registers/new/', views.device_type_register_detail,
         name='device_type_register_detail_new'),
    path('devices/<int:device_id>/registers/', views.device_registers,
         name='device_registers'),
]
