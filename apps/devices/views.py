from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.template import loader
from django.shortcuts import get_object_or_404, render

from .forms import DeviceForm, DeviceTypeForm, DeviceTypeRegisterForm
from .models import Device, DeviceType, DeviceTypeRegister, RecordTypes, \
    ReadFunctionCodes, WriteFunctionCodes, RegisterDataType
from apps.tasks.models import ReadTaskStatus, ReadTask


def devices(request):
    devices_list = list(Device.objects.order_by('port'))
    template = loader.get_template('devices/devices_list.html')

    for device in devices_list:
        device.record_type = RecordTypes.values[device.record_type]

    context = {
        'devices_list': devices_list,
    }
    return HttpResponse(template.render(context, request))


def device_detail(request, device_id=None):
    if device_id:
        device = get_object_or_404(Device, pk=device_id)
        device_title = device.name
    else:
        device = Device()
        device_title = None

    if request.method == 'POST':
        form = DeviceForm(request.POST, instance=device)

        if form.is_valid():
            device.name = form.cleaned_data['name']
            device.address = form.cleaned_data['address']
            device.port = form.cleaned_data['port']
            device.type = form.cleaned_data['type']
            device.record_type = form.cleaned_data['record_type']
            device.comment = form.cleaned_data['comment']

            # print(device.id)
            # print(device.name)
            # print(device.address)
            # print(device.port)
            # print(device.type)
            # print(device.record_type)
            # print(device.comment)

            # save into base
            device.save()

            return HttpResponseRedirect(reverse('devices:devices'))
    else:
        form = DeviceForm(initial={'name': device.name, 'address': device.address, 'port': device.port,
                                   'type': device.type, 'record_type': device.record_type,
                                   'comment': device.comment})

    return render(request, 'devices/device_detail.html', {'form': form, 'device': device, 'device_title': device_title})


def device_types(request):
    device_types_list = list(DeviceType.objects.order_by('name'))
    template = loader.get_template('devices/device_types_list.html')

    context = {
        'device_types_list': device_types_list,
    }
    return HttpResponse(template.render(context, request))


def device_type_detail(request, device_type_id=None):
    if device_type_id:
        device_type = get_object_or_404(DeviceType, pk=device_type_id)
        device_type_title = device_type.name
    else:
        device_type = DeviceType()
        device_type_title = None

    if request.method == 'POST':
        form = DeviceTypeForm(request.POST, instance=device_type)

        if form.is_valid():
            device_type.name = form.cleaned_data['name']
            device_type.table_name = form.cleaned_data['table_name']
            device_type.comment = form.cleaned_data['comment']

            # print(device_type.id)
            # print(device_type.name)
            # print(device_type.table_name)
            # print(device_type.comment)

            # save into base
            device_type.save()

            return HttpResponseRedirect(reverse('devices:device_types'))
    else:
        form = DeviceTypeForm(initial={'name': device_type.name, 'table_name': device_type.table_name,
                                       'comment': device_type.comment})

    return render(request, 'devices/device_type_detail.html', {'form': form, 'device_type': device_type,
                                                               'device_type_title': device_type_title})


def device_type_registers(request, device_type_id):
    registers = list(DeviceTypeRegister.objects.filter(device_type_id=device_type_id).order_by('name'))
    device_type = list(DeviceType.objects.filter(id=device_type_id))[0]
    template = loader.get_template('devices/registers_list.html')

    for register in registers:
        register.function = ReadFunctionCodes.values[register.function]

    for register in registers:
        register.write_function = WriteFunctionCodes.values[register.write_function]

    for register in registers:
        register.byte_count = RegisterDataType.values[register.byte_count]

    context = {
        'registers_list': registers,
        'device_type': device_type,
    }
    return HttpResponse(template.render(context, request))


def device_type_register_detail(request, device_type_id, register_id=None):
    device_type = get_object_or_404(DeviceType, pk=device_type_id)

    if register_id:
        register = get_object_or_404(DeviceTypeRegister, pk=register_id)
        register_title = register.name
    else:
        register = DeviceTypeRegister()
        register.device_type = device_type
        register_title = None

    if request.method == 'POST':
        form = DeviceTypeRegisterForm(request.POST, instance=register, initial={'device_type': device_type})

        if form.is_valid():
            register.device_type = form.cleaned_data['device_type']
            register.name = form.cleaned_data['name']
            register.address = form.cleaned_data['address']
            register.function = form.cleaned_data['function']
            register.write_function = form.cleaned_data['write_function']
            register.byte_count = form.cleaned_data['byte_count']
            register.comment = form.cleaned_data['comment']

            # print(register.id)
            # print(register.device_type)
            # print(register.name)
            # print(register.address)
            # print(register.function)
            # print(register.write_function)
            # print(register.byte_count)
            # print(register.comment)

            # save into base
            register.save()

            return HttpResponseRedirect(reverse('devices:device_type_registers', args=[device_type_id, ]))
    else:
        form = DeviceTypeRegisterForm(initial={'device_type': device_type, 'name': register.name,
                                               'address': register.address,
                                               'function': register.function, 'write_function': register.write_function,
                                               'byte_count': register.byte_count, 'comment': register.comment})

    return render(request, 'devices/register_detail.html', {'form': form, 'register': register,
                                                            'register_title': register_title})


def device_delete(request, device_id):
    device = get_object_or_404(Device, pk=device_id)
    device.delete()

    return HttpResponseRedirect(reverse('devices:devices'))


def device_type_delete(request, device_type_id):
    device_type = get_object_or_404(DeviceType, pk=device_type_id)

    read_tasks = ReadTask.objects.filter(device__type_id=device_type_id, status=ReadTaskStatus.ENABLED)
    for task in read_tasks:
        task.status = ReadTaskStatus.DISABLED
        task.save()

    device_type.delete()

    return HttpResponseRedirect(reverse('devices:device_types'))


def device_type_register_delete(request, device_type_id, register_id):
    register = get_object_or_404(DeviceTypeRegister, pk=register_id)
    register.delete()

    return HttpResponseRedirect(reverse('devices:device_type_registers', args=[device_type_id, ]))


def device_registers(request, device_id):
    registers = []
    if device_id:
        device = get_object_or_404(Device, pk=device_id)
        registers_list = list(DeviceTypeRegister.objects.filter(device_type_id=device.type.id))

        for register in registers_list:
            registers.append({'id': register.id, 'value': str(register), 'w_function': register.write_function,
                              'r_function': register.function})

    return JsonResponse({'registers': registers})
