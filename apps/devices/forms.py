from django import forms
from django.core.exceptions import ValidationError

from .models import Device, DeviceType, DeviceTypeRegister


class DeviceForm(forms.ModelForm):
    def clean_port(self):
        port = self.cleaned_data['port']
        address = self.cleaned_data['address']
        device_exists = Device.objects.filter(port_id=port.id, address=address).exclude(pk=self.instance.pk).exists()
        if not device_exists:
            return port
        else:
            raise ValidationError("Device with this Port and Self address of device (id, integer) already exists.")

    class Meta:
        model = Device
        fields = ['name', 'address', 'port', 'type', 'record_type', 'comment']
        # exclude = ['address']


class DeviceTypeForm(forms.ModelForm):
    class Meta:
        model = DeviceType
        fields = ['name', 'table_name', 'comment']


class DeviceTypeRegisterForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(DeviceTypeRegisterForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance:
            self.fields['device_type'].widget.attrs['hidden'] = True
            self.fields['device_type'].label = ''

    def clean_device_type(self):
        instance = getattr(self, 'instance', None)
        if instance:
            return instance.device_type
        else:
            return self.cleaned_data['device_type']

    class Meta:
        model = DeviceTypeRegister
        fields = ['device_type', 'name', 'address', 'function', 'write_function', 'byte_count', 'comment']
