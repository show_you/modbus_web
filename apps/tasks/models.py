from django.db import models
from apps.common.base_enum import BaseEnumerate


class ErrorReportPlans(BaseEnumerate):
    u"""
    Plans of error reporting
    """

    (
        DONT_REPORT, REPORT_ALL, REPORT_DB, REPORT_LOG
    ) = range(4)

    values = {
        DONT_REPORT: u'Dont report errors',
        REPORT_ALL: u'Report errors in DB and logs',
        REPORT_DB: u'Report errors in DB',
        REPORT_LOG: u'Report errors in logs'
    }


class ReadTaskStatus(BaseEnumerate):
    u"""
    Task status: enabled/disabled
    """

    (
        ENABLED, DISABLED
    ) = range(2)

    values = {
        ENABLED: u'Task is enabled',
        DISABLED: u'Task is disabled'
    }


class WriteTaskStatus(BaseEnumerate):
    u"""
    Task status: enabled/disabled
    """

    (
        ENABLED, DISABLED, SUCCESS, ERROR
    ) = range(4)

    values = {
        ENABLED: u'Task is enabled',
        DISABLED: u'Task is disabled',
        SUCCESS: u'Task has successful result',
        ERROR: u'Task is finished with errors'
    }


class ReadTask(models.Model):
    u"""
    Task to read specific register from some device
    """

    device = models.OneToOneField(
        'devices.Device', verbose_name=u'Device', on_delete=models.CASCADE
    )

    interval = models.PositiveIntegerField(
        u'polling period, s',
        default=60,
        help_text=u'seconds',
        blank=False, null=False
    )

    data_miss_error_report = models.PositiveIntegerField(
        u'report plan on missing data error',
        choices=ErrorReportPlans.get_choices(),
        default=ErrorReportPlans.REPORT_LOG
    )

    status = models.PositiveIntegerField(
        u'task status',
        choices=ReadTaskStatus.get_choices(),
        default=ReadTaskStatus.DISABLED
    )

    class Meta:
        verbose_name = u'read task'
        verbose_name_plural = u'read tasks'
        ordering = ['device']
        db_table = 'poll_read_tasks'

    def __str__(self):
        return u'{0} read task'.format(self.device.name)


class WriteTask(models.Model):
    u"""
    Task to write specific register to some device
    """

    device = models.ForeignKey(
        'devices.Device', verbose_name=u'Device', on_delete=models.CASCADE
    )

    register = models.ForeignKey(
        'devices.DeviceTypeRegister', verbose_name=u'Register', on_delete=models.CASCADE
    )

    value = models.CharField(
        u'value to set to register',
        max_length=20,
        help_text=u'String representation! Be cautious!',
        blank=False, null=False
    )

    status = models.PositiveIntegerField(
        u'task status',
        choices=WriteTaskStatus.get_choices(),
        default=WriteTaskStatus.DISABLED
    )

    changed_datetime = models.DateTimeField(
        u'time point of last modification',
        auto_now=True
    )

    class Meta:
        verbose_name = u'write task'
        verbose_name_plural = u'write tasks'
        ordering = ['device', 'register']
        db_table = 'poll_write_tasks'

    def __str__(self):
        return u'Write task {0} register {1} value {2}'.format(self.device.name, self.register.name, self.value)


class Threshold(models.Model):
    u"""
    Threshold for some device register, used to track by task
    """

    task = models.ForeignKey(
        'ReadTask', verbose_name=u'Task', on_delete=models.CASCADE
    )

    register = models.ForeignKey(
        'devices.DeviceTypeRegister', verbose_name=u'Register', on_delete=models.CASCADE
    )

    # FloatField instead of DecimalField because of firebird problems
    low_threshold = models.FloatField(
        u'low threshold',
        blank=True, null=True
    )

    # FloatField instead of DecimalField because of firebird problems
    high_threshold = models.FloatField(
        u'high threshold',
        blank=True, null=True
    )

    additive = models.FloatField(
        u'additive modifier',
        blank=True, null=True, default=0
    )

    coefficient = models.FloatField(
        u'coefficient modifier',
        blank=True, null=True, default=1
    )

    check_mask = models.PositiveIntegerField(
        u'check logical equality with mask, bool(x & mask), if true => alert',
        help_text=u'positive integer value',
        blank=True, null=True
    )

    mask_explain = models.CharField(
        u'explanation of mask check',
        max_length=30,
        help_text=u'explanation of mask check (for alert message)',
        blank=True, null=True
    )

    threshold_error_report = models.PositiveIntegerField(
        u'report plan on threshold error',
        choices=ErrorReportPlans.get_choices(),
        default=ErrorReportPlans.REPORT_LOG
    )

    class Meta:
        verbose_name = u'threshold'
        verbose_name_plural = u'thresholds'
        # unique_together = ('task', 'register')
        ordering = ['task', 'register']
        db_table = 'poll_thresholds'

    def __str__(self):
        return u'Threshold of {0} register {1}'.format(self.task.device.name, self.register.name)
