from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.template import loader
from django.shortcuts import get_object_or_404, render

from .forms import ReadTaskForm, WriteTaskForm, ThresholdForm
from .models import ReadTask, WriteTask, Threshold, ReadTaskStatus, WriteTaskStatus, ErrorReportPlans


def read_tasks(request):
    tasks_list = list(ReadTask.objects.order_by('device__name'))
    template = loader.get_template('tasks/read_tasks_list.html')

    for task in tasks_list:
        task.data_miss_error_report = ErrorReportPlans.values[task.data_miss_error_report]

    for task in tasks_list:
        task.status_str = ReadTaskStatus.values[task.status]

    context = {
        'tasks_list': tasks_list,
    }
    return HttpResponse(template.render(context, request))


def read_task_detail(request, task_id=None):
    if task_id:
        task = get_object_or_404(ReadTask, pk=task_id)
        task_title = task.device.name
    else:
        task = ReadTask()
        task_title = None

    if request.method == 'POST':
        form = ReadTaskForm(request.POST, instance=task)

        if form.is_valid():
            task.device = form.cleaned_data['device']
            task.interval = form.cleaned_data['interval']
            task.data_miss_error_report = form.cleaned_data['data_miss_error_report']
            task.status = form.cleaned_data['status']

            # print(task.id)
            # print(task.device)
            # print(task.interval)
            # print(task.data_miss_error_report)
            # print(task.status)

            # save into base
            task.save()

            return HttpResponseRedirect(reverse('tasks:read_tasks'))
    else:
        if task_id:
            form = ReadTaskForm(initial={'device': task.device, 'interval': task.interval,
                                         'data_miss_error_report': task.data_miss_error_report, 'status': task.status})
        else:
            form = ReadTaskForm(initial={'device': '', 'interval': task.interval,
                                         'data_miss_error_report': task.data_miss_error_report, 'status': task.status})

    return render(request, 'tasks/read_task_detail.html', {'form': form, 'task': task, 'task_title': task_title})


def task_thresholds(request, task_id):
    thresholds = list(Threshold.objects.filter(task_id=task_id).order_by('register__name'))
    task = list(ReadTask.objects.filter(id=task_id))[0]
    template = loader.get_template('tasks/thresholds_list.html')

    for threshold in thresholds:
        threshold.threshold_error_report = ErrorReportPlans.values[threshold.threshold_error_report]

    context = {
        'thresholds_list': thresholds,
        'task': task,
    }
    return HttpResponse(template.render(context, request))


def task_threshold_detail(request, task_id, threshold_id=None):
    task = get_object_or_404(ReadTask, pk=task_id)
    # register_list = list(DeviceTypeRegister.objects.filter(device_type_id=task.device.type))

    if threshold_id:
        threshold = get_object_or_404(Threshold, pk=threshold_id)
        threshold_title = threshold.register
    else:
        threshold = Threshold()
        threshold.task = task
        threshold_title = None

    if request.method == 'POST':
        form = ThresholdForm(request.POST, instance=threshold)

        if form.is_valid():
            threshold.task = form.cleaned_data['task']
            threshold.register = form.cleaned_data['register']
            threshold.low_threshold = form.cleaned_data['low_threshold']
            threshold.high_threshold = form.cleaned_data['high_threshold']
            threshold.threshold_error_report = form.cleaned_data['threshold_error_report']
            threshold.additive = form.cleaned_data['additive']
            threshold.coefficient = form.cleaned_data['coefficient']
            threshold.check_mask = form.cleaned_data['check_mask']
            threshold.mask_explain = form.cleaned_data['mask_explain']

            # print(threshold.id)
            # print(threshold.task)
            # print(threshold.register)
            # print(threshold.low_threshold)
            # print(threshold.high_threshold)
            # print(threshold.threshold_error_report)

            # save into base
            threshold.save()

            return HttpResponseRedirect(reverse('tasks:task_thresholds', args=[task_id, ]))
    else:
        if threshold_id:
            form = ThresholdForm(initial={'task': task, 'register': threshold.register,
                                          'low_threshold': threshold.low_threshold,
                                          'high_threshold': threshold.high_threshold,
                                          'threshold_error_report': threshold.threshold_error_report,
                                          'additive': threshold.additive,
                                          'coefficient': threshold.coefficient,
                                          'check_mask': threshold.check_mask,
                                          'mask_explain': threshold.mask_explain})
        else:
            form = ThresholdForm(initial={'task': task, 'register': '',
                                          'low_threshold': threshold.low_threshold,
                                          'high_threshold': threshold.high_threshold,
                                          'threshold_error_report': threshold.threshold_error_report,
                                          'additive': threshold.additive,
                                          'coefficient': threshold.coefficient,
                                          'check_mask': threshold.check_mask,
                                          'mask_explain': threshold.mask_explain})

    return render(request, 'tasks/threshold_detail.html', {'form': form, 'threshold': threshold,
                                                           'threshold_title': threshold_title})


def write_tasks(request):
    tasks_list = list(WriteTask.objects.order_by('-changed_datetime'))
    template = loader.get_template('tasks/write_tasks_list.html')

    for task in tasks_list:
        task.status_str = WriteTaskStatus.values[task.status]

    context = {
        'tasks_list': tasks_list,
    }
    return HttpResponse(template.render(context, request))


def write_task_detail(request, task_id=None):
    if task_id:
        task = get_object_or_404(WriteTask, pk=task_id)
        task_title = task.device.name
    else:
        task = WriteTask()
        task_title = None

    if request.method == 'POST':
        form = WriteTaskForm(request.POST, instance=task)

        if form.is_valid():
            task.device = form.cleaned_data['device']
            task.register = form.cleaned_data['register']
            task.value = form.cleaned_data['value']
            task.status = form.cleaned_data['status']

            # print(task.id)
            # print(task.device)
            # print(task.register)
            # print(task.value)
            # print(task.status)
            # print(task.changed_datetime)

            # save into base
            task.save()

            return HttpResponseRedirect(reverse('tasks:write_tasks'))
    else:
        if task_id:
            form = WriteTaskForm(initial={'device': task.device, 'register': task.register, 'value': task.value,
                                          'status': task.status})
        else:
            form = WriteTaskForm(initial={'device': '', 'register': '', 'value': task.value,
                                          'status': task.status})

    return render(request, 'tasks/write_task_detail.html', {'form': form, 'task': task, 'task_title': task_title})


def read_task_delete(request, task_id):
    task = get_object_or_404(ReadTask, pk=task_id)
    task.delete()

    return HttpResponseRedirect(reverse('tasks:read_tasks'))


def task_threshold_delete(request, task_id, threshold_id):
    threshold = get_object_or_404(Threshold, pk=threshold_id)
    threshold.delete()

    return HttpResponseRedirect(reverse('tasks:task_thresholds', args=[task_id, ]))


def write_task_delete(request, task_id):
    task = get_object_or_404(WriteTask, pk=task_id)
    task.delete()

    return HttpResponseRedirect(reverse('tasks:write_tasks'))


def read_task_toggle(request, task_id):
    changed = False
    status = ''
    read_task = ReadTask.objects.get(pk=task_id)

    if read_task.device.type and read_task.device.port:
        if read_task.status == ReadTaskStatus.ENABLED:
            read_task.status = ReadTaskStatus.DISABLED
        else:
            read_task.status = ReadTaskStatus.ENABLED
        read_task.save()
        status = ReadTaskStatus.values[read_task.status]
        changed = True

    return JsonResponse({'changed': changed, 'task_status': status})


def write_task_toggle(request, task_id):
    changed = False
    status = ''
    write_task = get_object_or_404(WriteTask, pk=task_id)

    if write_task.device.type and write_task.device.port:
        if write_task.status == WriteTaskStatus.ENABLED:
            write_task.status = WriteTaskStatus.DISABLED
        else:
            write_task.status = WriteTaskStatus.ENABLED
        write_task.save()
        status = WriteTaskStatus.values[write_task.status]
        changed = True

    return JsonResponse({'changed': changed, 'task_status': status})
