from modbus.settings import DATA_FILES_DIR, DATABASES, ERROR_DB
import threading
from apps.ports.models import PortTypes
from apps.tasks.models import ReadTaskStatus, WriteTaskStatus, ReadTask, WriteTask, Threshold
from apps.devices.models import *
from .instruments import InstrumentProvider, ModbusRTUInstrument, ModbusTCPInstrument
from time import sleep
from datetime import datetime, timedelta
import fdb
import psycopg2
from psycopg2 import Error
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import minimalmodbus
import logging.config
from math import isnan


def generate_error(device_name="", interval=2, message="", outer_logger=None):
    u"""
    Generate error into ERROR_DB
    :param outer_logger: logger from outside
    :param device_name: device name
    :param interval: interval device polling
    :param message: error message
    :return: None
    """

    if len(device_name) == 0:
        return

    try:
        connection = fdb.connect(dsn=ERROR_DB['dsn'], user=ERROR_DB['user'], password=ERROR_DB['password'])
        cursor = connection.cursor()
    except (fdb.ProgrammingError, fdb.DatabaseError) as e:
        if outer_logger is not None:
            outer_logger.error("Error during ERROR_DB connection: " + str(e))
        else:
            print("Error during ERROR_DB connection: " + str(e))
        return

    identity = []
    try:
        statement = "SELECT evch$devices.id from evch$devices where evch$devices.name = \'" + \
                    device_name + "\'"
        cursor.execute(statement)
        identity = cursor.fetchall()
        if len(identity) != 1:
            raise fdb.ProgrammingError("Cant find correct identity for device")
        else:
            device_id = identity[0][0]
    except (fdb.ProgrammingError, fdb.DatabaseError) as e:
        if outer_logger is not None:
            outer_logger.error(str(e))
        else:
            print(str(e))
        device_id = 'null'

    statement = ""
    try:
        statement = "EXECUTE BLOCK AS declare ErrId integer; " \
                    "BEGIN " \
                    "select first 1 id from evch$errors " \
                    "where (addsecond(stop_time, timeout) > \'NOW\' and " \
                    "message_short like \'{}\') into ErrId; " \
                    "if(ErrId is null) then " \
                    "insert into evch$errors " \
                    "values(0, \'NOW\', ADDSECOND(\'NOW\', {}), {}, null, {}, 8, \'{}\', \'{}\', \'{}\'); " \
                    "else " \
                    "update evch$errors " \
                    "set stop_time = ADDSECOND(\'NOW\', {}), message_short = \'{}\', message_long = \'{}\' " \
                    "where id = :ErrId; " \
                    "END".format(message.split(" ")[0] + " " + message.split(" ")[1] + "%",
                                 int(interval * 1.2), device_id, 5, message, "", "",
                                 int(interval * 1.2), message, "")
        cursor.execute(statement)
        connection.commit()
    except Exception as e:
        if outer_logger is not None:
            outer_logger.error("Error while generating error in DB : " + str(e))
            outer_logger.error(statement)
        else:
            print("Error while generating error in DB : " + str(e))
            print(statement)
    try:
        cursor.close()
        connection.close()
    except Exception as e:
        if outer_logger is not None:
            outer_logger.error("Error while closing DB connection : " + str(e))
        else:
            print("Error while closing DB connection : " + str(e))


def save_data_into_file(device_name="", data="", outer_logger=None):
    u"""
    Save device data into file with name = device_name
    :param outer_logger: logger from the outside
    :param device_name: name of the device to name the file to save data
    :param data: data to save
    :return: None
    """

    if len(device_name) == 0 or len(data) == 0:
        return
    try:
        with open(DATA_FILES_DIR + "/" + device_name + ".data", "a") as write_file:
            write_file.write(data + "\n")
    except Exception as err:
        if outer_logger is not None:
            outer_logger.error("Error while writing " + device_name + " data to file :" + str(err))
        else:
            print("Error while writing " + device_name + " data to file :" + str(err))


class ModbusPollThread:
    u"""
    Thread maintaining modbus polling
    """

    def __init__(self, name=None, speed=None):
        self.name = name
        self.speed = speed
        self.active = True
        # self.input_queue = None
        self.thread = threading.Thread(target=self.poll_data)
        self.thread.setDaemon(True)
        self.thread.start()

    def poll_data(self):
        # [{Task: {Device: {DeviceType: [Registers...]}}, {}]

        connection = 0
        cursor = 0
        db_fail = True
        instruments = []
        scheduler = {}

        # logger creating and initialization
        dict_log_config = {
            "version": 1,
            'disable_existing_loggers': False,
            "handlers": {
                "fileHandler_" + self.name: {
                    "class": "logging.handlers.RotatingFileHandler",
                    "formatter": "formatter_" + self.name,
                    "filename": "logs/thread_" + self.name + ".log",
                    "maxBytes": 20000000,
                    "backupCount": 5
                }
            },
            "loggers": {
                "logger_" + self.name: {
                    "handlers": ["fileHandler_" + self.name],
                    "level": "INFO",
                }
            },
            "formatters": {
                "formatter_" + self.name: {
                    "format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
                }
            }
        }
        logging.config.dictConfig(dict_log_config)
        poll_thread_logger = logging.getLogger("logger_" + self.name)

        poll_thread_logger.info("Polling thread for " + self.name + " is started")

        while self.active:
            try:
                # db check
                if db_fail:
                    try:
                        # print("Try to connect to DB")
                        if connection:
                            connection.close()

                        connection = psycopg2.connect(database=DATABASES['default']['NAME'],
                                                      host=DATABASES['default']['HOST'],
                                                      port=int(DATABASES['default']['PORT']),
                                                      user=DATABASES['default']['USER'],
                                                      password=DATABASES['default']['PASSWORD'])
                        connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
                        db_fail = False
                    except Error as e:
                        poll_thread_logger.error("Error during DB connection: " + str(e))
                        db_fail = True

                # port check
                try:
                    port = Port.objects.get(address=self.name)
                except Port.DoesNotExist as e:
                    poll_thread_logger.error("Port object " + self.name + " doesn't exist")
                    self.active = False
                    return

                if port.speed != self.speed:
                    self.speed = port.speed

                if port.type not in [PortTypes.MODBUS_TCP, PortTypes.MODBUS_RTU]:
                    poll_thread_logger.error("Port object of type " + PortTypes.values(port.type) +
                                             " is not supported")
                    self.active = False
                    return

                # get write and tasks with status ENABLED for current Port
                write_tasks = set(WriteTask.objects.filter(device__port__address=self.name)
                                  .filter(status=WriteTaskStatus.ENABLED))
                read_tasks = set(ReadTask.objects.filter(device__port__address=self.name)
                                 .filter(status=ReadTaskStatus.ENABLED))

                # update scheduler
                keys = list(scheduler.keys())
                for key in keys:
                    if key not in [task.id for task in read_tasks]:
                        del scheduler[key]
                for task in read_tasks:
                    if task.id not in scheduler.keys():
                        scheduler[task.id] = datetime.now()

                # read tasks check, prepare database tables and columns
                device_types = set(DeviceType.objects.filter(device__port__address=self.name)
                                   .filter(device__readtask__status=ReadTaskStatus.ENABLED)
                                   .filter(device__record_type=RecordTypes.DB_RECORD))
                for device_type in device_types:
                    result = []
                    statement_fields = "select column_name, data_type  from information_schema.columns " \
                                       "where table_name = '" + device_type.table_name.lower() + \
                                       "' AND table_schema = 'public'"
                    cursor = connection.cursor()
                    try:
                        cursor.execute(statement_fields)
                        result = cursor.fetchall()
                        if len(result) == 0:
                            raise Error("Table doesn't exist")
                    except Error as e:
                        poll_thread_logger.error("Error while getting fields of data table: " + str(e))
                        statement = ""
                        try:
                            statement = "CREATE TABLE " + device_type.table_name + \
                                        " (ID SERIAL, DATETIME_POINT TIMESTAMP NOT NULL," \
                                        " DEVICE_ID INTEGER NOT NULL)"
                            cursor.execute(statement)
                            statement = "ALTER TABLE " + device_type.table_name + " ADD  PRIMARY KEY (ID)"
                            cursor.execute(statement)
                            statement = "ALTER TABLE " + device_type.table_name + " ADD CONSTRAINT " + \
                                        device_type.table_name + \
                                        "_F_KEY FOREIGN KEY (DEVICE_ID) REFERENCES POLL_DEVICES (ID) on delete CASCADE"
                            cursor.execute(statement)
                            statement = "CREATE INDEX " + device_type.table_name + "_IDX ON " + \
                                        device_type.table_name + " (DATETIME_POINT)"
                            cursor.execute(statement)
                            cursor.execute(statement_fields)
                            result = cursor.fetchall()
                        except Error as e:
                            poll_thread_logger.error("Error while creating new table for data: " + str(e))
                            poll_thread_logger.info(statement)

                    if len(result) != 0:
                        for register in list(device_type.devicetyperegister_set.exclude(function=ReadFunctionCodes.NO_READ)):
                            if register.byte_count in [RegisterDataType.INTEGER, RegisterDataType.INT4]:
                                reg_type_str = 'integer'
                            else:
                                reg_type_str = 'double precision'
                            reg_tuple = (register.name.lower(), reg_type_str,)
                            # print(reg_tuple)
                            # print(result[0])
                            # print(reg_tuple[0])
                            # print(result[0][0])
                            if reg_tuple not in result:
                                if register.name.lower() in [field[0] for field in result]:
                                    statement = "ALTER TABLE " + device_type.table_name + " DROP " + register.name
                                    try:
                                        cursor.execute(statement)
                                    except Error as e:
                                        poll_thread_logger.error("Error while dropping column: " + str(e))
                                if register.byte_count in [RegisterDataType.INTEGER, RegisterDataType.INT4]:
                                    reg_type_str = 'INTEGER'
                                else:
                                    reg_type_str = 'DOUBLE PRECISION'
                                statement = "ALTER TABLE " + device_type.table_name + " ADD " + register.name + " "\
                                            + reg_type_str
                                try:
                                    cursor.execute(statement)
                                except Error as e:
                                    poll_thread_logger.error("Error while adding column: " + str(e))
                    else:
                        poll_thread_logger.error("Create table second error")

                    if cursor:
                        cursor.close()

                # devices instruments check
                devices = set(Device.objects.filter(port__address=self.name)
                              .filter(readtask__status=ReadTaskStatus.ENABLED))
                devices.union(set(Device.objects.filter(port__address=self.name)
                                  .filter(writetask__status=WriteTaskStatus.ENABLED)))

                instruments = [i for i in instruments if i.address in [d.address for d in devices]]
                for device in devices:
                    if device.address not in [i.address for i in instruments]:
                        try:
                            if port.type == PortTypes.MODBUS_RTU:
                                instrument = ModbusRTUInstrument(port=port.address, address=device.address, speed=port.speed)
                            else:
                                instrument = ModbusTCPInstrument(port=port.address, address=device.address)
                            instruments.append(instrument)
                        except Exception as err:
                            poll_thread_logger.error("Error while creating new instrument for " + device.name + " : " +
                                                     str(err))
                for instrument in instruments:
                    instrument.set_speed(port.speed)

                # run write tasks
                for task in write_tasks:
                    instrument_found = False
                    for instrument in instruments:
                        if instrument.check_address(task.device.address):
                            instrument_found = True
                            try:
                                if task.register.byte_count == RegisterDataType.INTEGER:
                                    try:
                                        instrument.write_register(task.register.address,
                                                                  int(float(task.value.replace(',', '.'))),
                                                                  task.register.write_function)
                                    except Exception as e:
                                        instrument.write_register(task.register.address,
                                                                  int(float(task.value.replace(',', '.'))),
                                                                  task.register.write_function)
                                elif task.register.byte_count == RegisterDataType.FLOAT:
                                    try:
                                        instrument.write_float(task.register.address,
                                                               float(task.value.replace(',', '.')))
                                    except Exception as e:
                                        instrument.write_float(task.register.address,
                                                               float(task.value.replace(',', '.')))
                                else:
                                    raise NotImplementedError("Not yet implemented feature to write register type " +
                                                              RegisterDataType.values(task.register.byte_count))
                                task.status = WriteTaskStatus.SUCCESS
                            except Exception as err:
                                poll_thread_logger.error("Error while writing register " + task.register.name +
                                                         " to device " + task.device.name + " error: " + str(err))
                                task.status = WriteTaskStatus.ERROR
                            break
                    if not instrument_found:
                        poll_thread_logger.error("No instrument for device " + task.device.name)
                        task.status = WriteTaskStatus.ERROR
                    task.save()

                # check time using scheduler and run read tasks
                for task in read_tasks:
                    if datetime.now().timestamp() > scheduler[task.id].timestamp():
                        # execute task
                        sql_statement = "INSERT INTO " + task.device.type.table_name
                        temp_values = ""
                        temp_columns = ""
                        sql_statement_ready = False

                        instrument = None
                        for i in instruments:
                            if i.address == task.device.address:
                                instrument = i
                                break
                        if instrument is None:
                            if task.data_miss_error_report == ErrorReportPlans.REPORT_ALL or \
                                    task.data_miss_error_report == ErrorReportPlans.REPORT_DB:
                                generate_error(task.device.name, task.interval, task.device.name +
                                               " Fail to create connection, check port and device configurations",
                                               poll_thread_logger)
                            if task.data_miss_error_report == ErrorReportPlans.REPORT_ALL or \
                                    task.data_miss_error_report == ErrorReportPlans.REPORT_LOG:
                                poll_thread_logger.error("Error: there is no instrument for " + task.device.name)

                            # update time scheduler of next task execution
                            scheduler[task.id] += timedelta(seconds=task.interval)
                            continue

                        for register in set(task.device.type.devicetyperegister_set.all()):
                            thresholds = []
                            try:
                                if register.function != ReadFunctionCodes.NO_READ:
                                    # read register value
                                    if register.function == ReadFunctionCodes.READ_DISCRETE_INPUTS:
                                        try:
                                            value = instrument.read_bit(register.address)
                                        except Exception as e:
                                            value = instrument.read_bit(register.address)
                                        if not isinstance(value, int):
                                            raise AttributeError("value " + str(value) + " is not instance of int")
                                    elif register.byte_count in [RegisterDataType.INTEGER, RegisterDataType.INT4] and\
                                            register.function in [ReadFunctionCodes.READ_INPUT_REGISTERS,
                                                                  ReadFunctionCodes.READ_HOLDING_REGISTERS]:
                                        if register.byte_count == RegisterDataType.INT4:
                                            reg_count = 2
                                        else:
                                            reg_count = 1
                                        try:
                                            value = instrument.read_int(register.address, register.function, reg_count)
                                        except Exception as e:
                                            value = instrument.read_int(register.address, register.function, reg_count)
                                        if not isinstance(value, int):
                                            raise AttributeError("value " + str(value) + " is not instance of int")
                                    else:
                                        try:
                                            value = instrument.read_float(register.address, register.function)
                                        except Exception as e:
                                            value = instrument.read_float(register.address, register.function)
                                        if not isinstance(value, float) or isnan(value):
                                            raise AttributeError("value " + str(value) + " is not instance of float")

                                    # check thresholds
                                    try:
                                        thresholds = list(task.threshold_set.filter(register_id=register.id))
                                        for threshold in thresholds:
                                            messages = []

                                            # apply modifications
                                            modified_value = value
                                            if threshold.additive is not None:
                                                modified_value += threshold.additive
                                            if threshold.coefficient is not None:
                                                modified_value *= threshold.coefficient

                                            # check thresholds
                                            if threshold.high_threshold is not None \
                                                    and threshold.high_threshold < modified_value:
                                                messages.append("" + task.device.name + " " + register.name + " = " +
                                                                str(modified_value) + " > THRESHOLD HIGH = " +
                                                                str(threshold.high_threshold))
                                            if threshold.low_threshold is not None \
                                                    and threshold.low_threshold > modified_value:
                                                messages.append("" + task.device.name + " " + register.name + " = " +
                                                                str(modified_value) + " < THRESHOLD LOW = " +
                                                                str(threshold.low_threshold))

                                            # check mask
                                            if threshold.check_mask is not None \
                                                    and (int(modified_value) & threshold.check_mask):
                                                if threshold.mask_explain is not None \
                                                        and len(threshold.mask_explain) != 0:
                                                    messages.append("" + task.device.name + " " + register.name + " " +
                                                                    threshold.mask_explain)
                                                else:
                                                    messages.append("" + task.device.name + " " + register.name +
                                                                    " = " + str(modified_value) + " not masked " +
                                                                    str(threshold.check_mask))

                                            # report if errors
                                            if len(messages) != 0 and \
                                                    threshold.threshold_error_report != ErrorReportPlans.DONT_REPORT:
                                                if threshold.threshold_error_report == ErrorReportPlans.REPORT_DB or \
                                                        threshold.threshold_error_report == ErrorReportPlans.REPORT_ALL:
                                                    for message in messages:
                                                        generate_error(task.device.name, task.interval, message,
                                                                       poll_thread_logger)
                                                if threshold.threshold_error_report == ErrorReportPlans.REPORT_LOG or \
                                                        threshold.threshold_error_report == ErrorReportPlans.REPORT_ALL:
                                                    for message in messages:
                                                        poll_thread_logger.error("Threshold error : " + message)
                                    except Threshold.DoesNotExist as e:
                                        pass

                                    # concatenate statement string
                                    temp_values += "," + str(value)
                                    temp_columns += "," + register.name

                                    sql_statement_ready = True
                            except Exception as err:
                                if task.data_miss_error_report != ErrorReportPlans.DONT_REPORT:
                                    #     or (threshold != 0 and
                                    #         threshold.threshold_error_report != ErrorReportPlans.DONT_REPORT):
                                    if task.data_miss_error_report == ErrorReportPlans.REPORT_DB or \
                                            task.data_miss_error_report == ErrorReportPlans.REPORT_ALL:
                                        #      or (threshold != 0 and
                                        #      (threshold.threshold_error_report == ErrorReportPlans.REPORT_ALL or
                                        #       threshold.threshold_error_report == ErrorReportPlans.REPORT_DB)):
                                        generate_error(task.device.name, task.interval,
                                                       task.device.name + " " + register.name + " data is missing",
                                                       poll_thread_logger)
                                    if task.data_miss_error_report == ErrorReportPlans.REPORT_LOG or \
                                            task.data_miss_error_report == ErrorReportPlans.REPORT_ALL:
                                        #     or (threshold != 0 and
                                        #      (threshold.threshold_error_report == ErrorReportPlans.REPORT_LOG or
                                        #       threshold.threshold_error_report == ErrorReportPlans.REPORT_ALL)):
                                        poll_thread_logger.error("Error while trying to get " + register.name +
                                                                 " data from " + task.device.name + " : " + str(err))
                                temp_values += ",NULL"
                                temp_columns += "," + register.name

                        temp_values = "'" + str(datetime.now())[0:19] + "'," + str(task.device_id) + temp_values
                        temp_columns = "DATETIME_POINT,DEVICE_ID" + temp_columns
                        sql_statement += " (" + temp_columns + ") VALUES (" + temp_values + ")"

                        if not sql_statement_ready:
                            if task.data_miss_error_report == ErrorReportPlans.REPORT_ALL or \
                                    task.data_miss_error_report == ErrorReportPlans.REPORT_DB:
                                generate_error(task.device.name, task.interval, task.device.name + " DATA is missing",
                                               poll_thread_logger)
                            if task.data_miss_error_report == ErrorReportPlans.REPORT_ALL or \
                                    task.data_miss_error_report == ErrorReportPlans.REPORT_LOG:
                                poll_thread_logger.error("Error while trying to get data from " + task.device.name)
                        else:
                            # save data
                            if task.device.record_type == RecordTypes.DB_RECORD:
                                try:
                                    if cursor:
                                        cursor.close()
                                    cursor = connection.cursor()
                                    cursor.execute(sql_statement)
                                    cursor.close()
                                except Exception as err:
                                    if cursor:
                                        cursor.close()
                                    db_fail = True
                                    poll_thread_logger.error("Error while trying to save " + task.device.name +
                                                             " data into DB : " + str(err))
                                    save_data_into_file(task.device.name, sql_statement + ";", poll_thread_logger)
                            elif task.device.record_type == RecordTypes.FILE_RECORD:
                                save_data_into_file(task.device.name, sql_statement + ";", poll_thread_logger)

                        # update time scheduler of next task execution
                        scheduler[task.id] += timedelta(seconds=task.interval)

                # TODO: delete next string in prod, it is only for test purpose
                # poll_thread_logger.info("Tick " + self.name)

                sleep(1)
            except Exception as e:
                poll_thread_logger.error("Critical error : " + str(e))
                generate_error("modbus_web", 30,
                               "modbus_web Critical error",
                               poll_thread_logger)
                if cursor:
                    cursor.close()
                if connection:
                    connection.close()
                db_fail = True
                # need to sleep in case of critical db errors
                sleep(10)

        if cursor:
            cursor.close()
        if connection:
            connection.close()


class PortTasksProvider:
    u"""
    Handle separated threads for each port to process read/write tasks
    """

    def __init__(self):
        self.poll_threads = []

    def run_port_threads(self):
        while True:
            # check current port threads, if their flag active is False => delete port thread
            self.poll_threads = [x for x in self.poll_threads if x.active]

            # get from active tasks<-devices<-ports => active ports
            active_ports_set = set(Port.objects.filter(device__readtask__status=ReadTaskStatus.ENABLED))
            active_ports_set.union(set(Port.objects.filter(device__writetask__status=WriteTaskStatus.ENABLED)))

            for port in active_ports_set:
                # check if we currently have appropriate port thread
                if port.type in [PortTypes.MODBUS_RTU, PortTypes.MODBUS_TCP] \
                        and port.address not in [thread.name for thread in self.poll_threads]:
                    # if we have port thread => do nothing, else create port thread
                    self.poll_threads.append(ModbusPollThread(name=port.address, speed=port.speed))

            # wait some time (maybe 10 seconds)
            sleep(10.0)
            # repeat iteration
