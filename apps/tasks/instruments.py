import struct
import minimalmodbus
from pyModbusTCP.client import ModbusClient


class InstrumentProvider:
    def __init__(self, port, address, speed=None):
        self.port = port
        self.address = address
        self.speed = speed

    def write_register(self, reg_address, reg_value, f_code):
        raise NotImplementedError()

    def write_float(self, reg_address, reg_value):
        raise NotImplementedError()

    def write_registers(self, reg_address, reg_values):
        raise NotImplementedError()

    def read_registers(self, reg_address, f_code, reg_count=1):
        raise NotImplementedError()

    def read_register(self, reg_address, f_code):
        raise NotImplementedError()

    def read_bit(self, reg_address):
        raise NotImplementedError()

    def read_float(self, reg_address, f_code):
        raise NotImplementedError()

    def read_int(self, reg_address, f_code, reg_count=1):
        raise NotImplementedError()

    def set_speed(self, speed=None):
        pass

    def check_address(self, address):
        return self.address == address


class ModbusRTUInstrument(InstrumentProvider):
    def __init__(self, port, address, speed=None):
        super().__init__(port, address, speed)
        self.instrument = minimalmodbus.Instrument(port=port, slaveaddress=address)
        if speed is None:
            self.speed = 9600
        self.instrument.serial.baudrate = self.speed
        self.instrument.precalculate_read_size = True
        self.instrument.serial.timeout = 2

    def set_speed(self, speed=None):
        if speed != self.instrument.serial.baudrate:
            self.speed = speed
            self.instrument.serial.baudrate = speed

    def write_register(self, reg_address, reg_value, f_code):
        self.instrument.write_register(reg_address, reg_value, functioncode=f_code)

    def write_float(self, reg_address, reg_value):
        self.instrument.write_float(reg_address, reg_value)

    def write_registers(self, reg_address, reg_values):
        self.instrument.write_registers(reg_address, reg_values)

    def read_register(self, reg_address, f_code):
        return self.instrument.read_register(reg_address, functioncode=f_code)

    def read_registers(self, reg_address, f_code, reg_count=1):
        return self.instrument.read_registers(reg_address, reg_count, functioncode=f_code)

    def read_bit(self, reg_address):
        return self.instrument.read_bit(reg_address)

    def read_float(self, reg_address, f_code):
        return self.instrument.read_float(reg_address, functioncode=f_code)

    def read_int(self, reg_address, f_code, reg_count=1):
        if reg_count == 1:
            return self.read_register(reg_address, f_code)
        else:
            raise NotImplementedError()


class ModbusTCPInstrument(InstrumentProvider):
    def __init__(self, port, address, speed=None):
        super().__init__(port, address, speed)
        self.instrument = ModbusClient(host=port, port=502, auto_open=True, auto_close=True)

    def write_register(self, reg_address, reg_value, f_code):
        self.instrument.write_single_register(reg_address, reg_value)

    def write_float(self, reg_address, reg_value):
        value = bytearray(struct.pack("f", reg_value))
        self.write_registers(reg_address, value)

    def write_registers(self, reg_address, reg_values):
        self.instrument.write_multiple_registers(reg_address, reg_values)

    def read_register(self, reg_address, f_code):
        result = self.read_registers(reg_address, f_code, 1)
        if isinstance(result, list) and len(result) > 0:
            return result[0]
        else:
            return result

    def read_registers(self, reg_address, f_code, reg_count=1):
        if f_code == 3:
            result = self.instrument.read_holding_registers(reg_address, reg_count)
        elif f_code == 4:
            result = self.instrument.read_input_registers(reg_address, reg_count)
        elif f_code == 1:
            result = self.instrument.read_coils(reg_address, reg_count)
        elif f_code == 2:
            result = self.instrument.read_discrete_inputs(reg_address, reg_count)
        else:
            raise NotImplementedError()
        return result

    def read_bit(self, reg_address):
        result = self.read_registers(reg_address, 2, 1)[0]
        if isinstance(result, list) and len(result) > 0:
            return int(result[0])
        else:
            return result

    def read_float(self, reg_address, f_code):
        result = self.read_registers(reg_address, f_code, 2)
        bytes_list = [y for x in [[e & 0xFF, (e >> 8) & 0xFF] for e in result if e] for y in x]
        return struct.unpack('<f', bytearray(bytes_list))[0]

    def read_int(self, reg_address, f_code, reg_count=1):
        result = self.read_registers(reg_address, f_code, reg_count)
        bytes_list = [y for x in [[e & 0xFF, (e >> 8) & 0xFF] for e in result if e] for y in x]
        return int.from_bytes(bytes_list, byteorder='little')
