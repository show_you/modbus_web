from django.core.management.base import BaseCommand

from apps.tasks.core import PortTasksProvider


class Command(BaseCommand):
    help = u'Port task provider'

    def handle(self, *args, **options):
        provider = PortTasksProvider()
        provider.run_port_threads()
