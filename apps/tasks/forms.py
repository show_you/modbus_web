from django import forms
from django.core.exceptions import ValidationError

from apps.devices.models import DeviceTypeRegister
from .models import ReadTask, WriteTask, Threshold


class ReadTaskForm(forms.ModelForm):
    class Meta:
        model = ReadTask
        fields = ['device', 'interval', 'data_miss_error_report', 'status']


class WriteTaskForm(forms.ModelForm):
    def clean(self):
        cleaned_data = super().clean()
        device = cleaned_data.get("device")
        register = cleaned_data.get("register")

        if device and register:
            if len(list(DeviceTypeRegister.objects.filter(device_type__device__id=device.id, id=register.id))) == 0:
                raise ValidationError('Register does not belong to this device')

    class Meta:
        model = WriteTask
        fields = ['device', 'register', 'value', 'status']
        exclude = ['changed_datetime']


class ThresholdForm(forms.ModelForm):
    # register = forms.ModelChoiceField(queryset=None)

    def __init__(self, *args, **kwargs):
        super(ThresholdForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance:
            self.fields['task'].widget.attrs['hidden'] = True
            self.fields['task'].label = ''
            # self.fields['register'].queryset = DeviceTypeRegister.objects.all()

    def clean_device_type(self):
        instance = getattr(self, 'instance', None)
        if instance:
            return instance.task
        else:
            return self.cleaned_data['task']

    def clean(self):
        cleaned_data = super().clean()
        task = cleaned_data.get("task")
        register = cleaned_data.get("register")

        if task and register:
            if len(list(DeviceTypeRegister.objects.filter(device_type__device__id=task.device.id, id=register.id))) == 0:
                raise ValidationError('Register does not belong to this device')

    class Meta:
        model = Threshold
        fields = ['task', 'register', 'low_threshold', 'high_threshold', 'additive', 'coefficient', 'check_mask',
                  'mask_explain', 'threshold_error_report']
