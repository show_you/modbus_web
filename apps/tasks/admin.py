from django.contrib import admin
from .models import ReadTask, WriteTask, Threshold


admin.site.register(ReadTask)
admin.site.register(WriteTask)
admin.site.register(Threshold)
