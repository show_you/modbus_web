from django.urls import path
from . import views


app_name = 'tasks'

urlpatterns = [
    path('readtasks/', views.read_tasks, name='read_tasks'),
    path('readtasks/<int:task_id>/', views.read_task_detail, name='read_task_detail'),
    path('readtasks/<int:task_id>/delete/', views.read_task_delete, name='read_task_delete'),
    path('readtasks/new/', views.read_task_detail, name='read_task_detail_new'),
    path('readtasks/<int:task_id>/thresholds/', views.task_thresholds, name='task_thresholds'),
    path('readtasks/<int:task_id>/thresholds/<int:threshold_id>/', views.task_threshold_detail,
         name='task_threshold_detail'),
    path('readtasks/<int:task_id>/thresholds/<int:threshold_id>/delete/', views.task_threshold_delete,
         name='task_threshold_delete'),
    path('readtasks/<int:task_id>/thresholds/new/', views.task_threshold_detail, name='task_threshold_detail_new'),
    path('writetasks/', views.write_tasks, name='write_tasks'),
    path('writetasks/<int:task_id>/', views.write_task_detail, name='write_task_detail'),
    path('writetasks/<int:task_id>/delete/', views.write_task_delete, name='write_task_delete'),
    path('writetasks/new/', views.write_task_detail, name='write_task_detail_new'),

    path('readtasks/<int:task_id>/toggle/', views.read_task_toggle, name='read_task_toggle'),
    path('writetasks/<int:task_id>/toggle/', views.write_task_toggle, name='write_task_toggle'),
]
