delete from poll_thresholds where low_threshold=0 and high_threshold=0;
delete from poll_read_tasks where interval = 0;
delete from poll_write_tasks where "VALUE" like 'TEST%';
delete from poll_registers where name like 'TEST%';
delete from poll_devices where name like 'TEST%';
delete from poll_device_types where name like 'TEST%';
delete from poll_ports where address like 'TEST%';


insert into poll_ports (id, type, address, speed, comment) values (4, 1, 'TEST_PORT_0', 9600, 'TEST_PURPOSE');
insert into poll_ports (id, type, address, speed, comment) values (5, 1, 'TEST_PORT_1', 9600, 'TEST_PURPOSE');

insert into poll_device_types (id, name, table_name, comment) values (2, 'TEST_TYPE_0', 'TEST_TABLE_0', 'TEST_PURPOSE');
insert into poll_device_types (id, name, table_name, comment) values (3, 'TEST_TYPE_1', 'TEST_TABLE_1', 'TEST_PURPOSE');

insert into poll_devices (id, name, address, record_type, comment, port_id, type_id) values (10, 'TEST_DEVICE_0', 90, 1, 'TEST_PURPOSE', 4, 2);
insert into poll_devices (id, name, address, record_type, comment, port_id, type_id) values (11, 'TEST_DEVICE_1', 91, 1, 'TEST_PURPOSE', 4, 2);
insert into poll_devices (id, name, address, record_type, comment, port_id, type_id) values (12, 'TEST_DEVICE_2', 92, 1, 'TEST_PURPOSE', 5, 2);
insert into poll_devices (id, name, address, record_type, comment, port_id, type_id) values (13, 'TEST_DEVICE_3', 93, 1, 'TEST_PURPOSE', 4, 3);
insert into poll_devices (id, name, address, record_type, comment, port_id, type_id) values (14, 'TEST_DEVICE_4', 94, 1, 'TEST_PURPOSE', 5, 3);
insert into poll_devices (id, name, address, record_type, comment, port_id, type_id) values (15, 'TEST_DEVICE_5', 95, 1, 'TEST_PURPOSE', 5, 3);

insert into poll_registers (id, name, address, "FUNCTION", byte_count, comment, device_type_id, write_function) values (17, 'TEST_REGISTER_0', 90, 3, 2, 'TEST_PURPOSE', 2, 0);
insert into poll_registers (id, name, address, "FUNCTION", byte_count, comment, device_type_id, write_function) values (18, 'TEST_REGISTER_1', 91, 3, 4, 'TEST_PURPOSE', 2, 16);
insert into poll_registers (id, name, address, "FUNCTION", byte_count, comment, device_type_id, write_function) values (19, 'TEST_REGISTER_2', 92, 3, 2, 'TEST_PURPOSE', 3, 0);
insert into poll_registers (id, name, address, "FUNCTION", byte_count, comment, device_type_id, write_function) values (20, 'TEST_REGISTER_3', 93, 3, 4, 'TEST_PURPOSE', 3, 16);

insert into poll_write_tasks (id, "VALUE", status, changed_datetime, device_id, register_id) values (6, 'TEST_VALUE_0', 0, 'NOW', 10, 18);
insert into poll_write_tasks (id, "VALUE", status, changed_datetime, device_id, register_id) values (1, 'TEST_VALUE_1', 0, 'NOW', 11, 18);
insert into poll_write_tasks (id, "VALUE", status, changed_datetime, device_id, register_id) values (2, 'TEST_VALUE_2', 0, 'NOW', 12, 18);
insert into poll_write_tasks (id, "VALUE", status, changed_datetime, device_id, register_id) values (3, 'TEST_VALUE_3', 0, 'NOW', 13, 20);
insert into poll_write_tasks (id, "VALUE", status, changed_datetime, device_id, register_id) values (4, 'TEST_VALUE_4', 0, 'NOW', 14, 20);
insert into poll_write_tasks (id, "VALUE", status, changed_datetime, device_id, register_id) values (5, 'TEST_VALUE_5', 0, 'NOW', 15, 20);

insert into poll_read_tasks (id, interval, data_miss_error_report, status, device_id) values (10, 0, 3, 0, 10);
insert into poll_read_tasks (id, interval, data_miss_error_report, status, device_id) values (11, 0, 3, 0, 11);
insert into poll_read_tasks (id, interval, data_miss_error_report, status, device_id) values (12, 0, 3, 0, 12);
insert into poll_read_tasks (id, interval, data_miss_error_report, status, device_id) values (13, 0, 3, 0, 13);
insert into poll_read_tasks (id, interval, data_miss_error_report, status, device_id) values (14, 0, 3, 0, 14);
insert into poll_read_tasks (id, interval, data_miss_error_report, status, device_id) values (15, 0, 3, 0, 15);

insert into poll_thresholds (id, low_threshold, high_threshold, threshold_error_report, register_id, task_id) values (6, 0, 0, 0, 17, 10);
insert into poll_thresholds (id, low_threshold, high_threshold, threshold_error_report, register_id, task_id) values (1, 0, 0, 0, 17, 11);
insert into poll_thresholds (id, low_threshold, high_threshold, threshold_error_report, register_id, task_id) values (2, 0, 0, 0, 17, 12);
insert into poll_thresholds (id, low_threshold, high_threshold, threshold_error_report, register_id, task_id) values (3, 0, 0, 0, 19, 13);
insert into poll_thresholds (id, low_threshold, high_threshold, threshold_error_report, register_id, task_id) values (4, 0, 0, 0, 19, 14);
insert into poll_thresholds (id, low_threshold, high_threshold, threshold_error_report, register_id, task_id) values (5, 0, 0, 0, 19, 15);

commit;
