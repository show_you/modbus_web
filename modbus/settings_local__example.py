
LOCAL_SECRET_KEY = 't*w@86xy03z4e^kbgr3p1d67)4b9!(6#5ni+)7b0g9gdpq69k'

LOCAL_ALLOWED_HOSTS = ['localhost', '127.0.0.1', '192.168.1.198']

LOCAL_ERROR_DB = {
    'dsn': '192.168.1.152/3050:TEST',
    'user': 'SYSDBA',
    'password': ''
}

LOCAL_DATABASE = {
    'NAME': '',
    'USER': '',
    'PASSWORD': '',
    'HOST': '127.0.0.1',
    'PORT': '3050',
}

IS_DEBUG = False
