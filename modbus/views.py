from modbus.settings import ERROR_DB
from django.http import HttpResponse, JsonResponse
from django.template import loader
from django.shortcuts import get_object_or_404, render
import json
from datetime import datetime, timedelta

import fdb

from apps.devices.models import Device, DeviceSchemeLocation
from apps.tasks.models import ReadTaskStatus, ReadTask


def main(request):
    devices = list(Device.objects.order_by('port'))
    locations = list(DeviceSchemeLocation.objects.order_by('device'))
    template = loader.get_template('home.html')

    context = {
        'devices': devices,
        'locations': locations,
    }

    return HttpResponse(template.render(context, request))


def filter_devices_scheme(request):
    devices = []
    if request.method == 'POST':
        name = request.POST.get('name', '')
        d_type = request.POST.get('type', '')
        comment = request.POST.get('comment', '')
        devices_list = list(Device.objects.filter(name__icontains=name, type__name__icontains=d_type,
                                                  comment__icontains=comment))

        for device in devices_list:
            devices.append({'id': device.id, 'name': device.name, 'type': device.type.name})

    return JsonResponse({'devices': devices})


def update_devices_scheme(request):
    try:
        if request.method == 'POST':
            devices = request.POST.get('devices')
            if devices:
                devices = json.loads(devices)
            locations = list(DeviceSchemeLocation.objects.order_by('device'))
            for location in locations:
                if location.device_id not in [int(device['id']) for device in devices]:
                    location.delete()
                else:
                    for device in devices:
                        if location.device_id == device['id']:
                            changed = False
                            if location.coordinateX != device['x']:
                                location.coordinateX = device['x']
                                changed = True
                            if location.coordinateY != device['y']:
                                location.coordinateY = device['y']
                                changed = True
                            if changed:
                                location.save()
                            break

            for device in devices:
                if device['id'] not in [location.device_id for location in locations]:
                    new_location = DeviceSchemeLocation()
                    new_location.device_id = device['id']
                    new_location.coordinateX = device['x']
                    new_location.coordinateY = device['y']
                    new_location.save()

        return JsonResponse({'success': True})
    except Exception as e:
        return JsonResponse({'success': False, 'error': str(e)})


def errors_list(request):
    response_json = {'errors': [], 'status': [], 'success': False, 'fail': ''}

    if request.method == 'POST':
        devices = request.POST.getlist('devices[]')
        devices_list = list(Device.objects.filter(id__in=devices))

        if devices_list:

            for device in devices_list:
                if ReadTask.objects.filter(device_id=device.id).exists() \
                        and device.readtask.status == ReadTaskStatus.ENABLED:
                    response_json['status'].append({'id': device.id, 'enabled': True, 'fail': False})
                else:
                    response_json['status'].append({'id': device.id, 'enabled': False, 'fail': False})

            try:
                # print("Try to connect to DB")
                connection = fdb.connect(dsn=ERROR_DB['dsn'], user=ERROR_DB['user'], password=ERROR_DB['password'])

                statement = "select first 100 err.start_time, err.stop_time, err.message_short, err.message_long " \
                            "from evch$errors as err " \
                            "where substring(err.message_short from 1 for position(' ' in err.message_short)) in ("
                for device in devices_list:
                    statement += "'" + device.name + "', "
                statement = statement[:-2] + ") order by err.id desc"

                cursor = connection.cursor()
                cursor.execute(statement)
                identity = cursor.fetchall()

                for row in identity:
                    error = {'start': row[0], 'stop': row[1], 'name': row[2].split(' ')[0], 'short': row[2], 'long': row[3], 'fail': False}
                    response_json['errors'].append(error)
                    for device in devices_list:
                        if device.name == error['name']:
                            if error['stop'] > datetime.now():
                                error['fail'] = True
                                for status in response_json['status']:
                                    if status['id'] == device.id:
                                        status['fail'] = True
                                        break
                            break

                response_json['success'] = True
            except Exception as e:
                response_json['fail'] = str(e)
    else:
        response_json['success'] = True

    return JsonResponse(response_json)
