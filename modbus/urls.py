"""modbus URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView
from django.conf import settings
from django.conf.urls.static import static
from . import views


urlpatterns = [
    path('admin/', admin.site.urls),
]

urlpatterns += [
    path('devices/', include('apps.devices.urls')),
]

urlpatterns += [
    path('', include('apps.ports.urls')),
]

urlpatterns += [
    path('tasks/', include('apps.tasks.urls')),
]

urlpatterns += [
    path('', include('apps.devicedata.urls')),
]

urlpatterns += [
    path('', views.main, name='main'),
    path('devices_scheme/filtered', views.filter_devices_scheme, name='filter_devices_scheme'),
    path('devices_scheme/update', views.update_devices_scheme, name='update_devices_scheme'),
    path('errors/list', views.errors_list, name='errors_list'),
]

urlpatterns += [
    path('', RedirectView.as_view(url='/', permanent=True)),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
