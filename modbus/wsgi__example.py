"""
WSGI config for modbus project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os
import sys
import site

from django.core.wsgi import get_wsgi_application

site.addsitedir('F:/python_workspace/django_projects/modbus_app/env/Lib/site-packages')

sys.path.append('F:/python_workspace/django_projects/modbus_app/env/scripts')
sys.path.append('F:/python_workspace/django_projects/modbus_app/modbus')
sys.path.append('F:/python_workspace/django_projects/modbus_app')

os.environ['DJANGO_SETTINGS_MODULE'] = 'modbus.settings'


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'modbus.settings')

application = get_wsgi_application()
